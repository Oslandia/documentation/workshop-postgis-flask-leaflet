from flask import Flask
from psycopg2 import connect

app = Flask(__name__)

@app.route('/geojson')
def geojson():
    with connect("service=workshop") as con:
        cur = con.cursor()
        cur.execute("""
            select json_build_object(
                'type', 'FeatureCollection',
                'features', json_agg(ST_AsGeoJSON(verre.*)::json)
            ) as geojson
            from verre
            """)
        return cur.fetchone()[0]

@app.route('/<path:path>')
def send_file(path):
    return app.send_static_file(path)

if __name__=="__main__":
    app.run(host='0.0.0.0', port='5000')
