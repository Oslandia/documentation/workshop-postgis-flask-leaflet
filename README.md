Ceci est le README du projet pour les développeurs, pour ceux qui on déjà récupéré la machine virtuelle, le tutoriel et [ici](https://gitlab.com/Oslandia/documentation/workshop-postgis-flask-leaflet/-/blob/master/roles/workshop/files/workshop/README.md)


# Installation du workshop en mode dév

Installer ansible dans un environnement virtuel

```sh
virtualenv -p python3 venv
. venv/bin/activate
python3 -m pip install ansible
```

Ensuite récupérer la machine virtuelle et la configurer
```
vagrant up
vagrant provision
```

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg



